/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */


/* piz.y */

/*-----------------------------------------------------------------------
    A bison syntax spec for Iz, a subset of Wiz.
    For use in the COMP90045 project 2014.
    Harald Sondergaard, March 2014.
-----------------------------------------------------------------------*/

%{

#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
#include "std.h"
#include "missing.h"
#include "list.h"
#include "symbol.h"

extern Program parsed_program;
extern void    report_error_and_exit(const char *msg);
extern char    *yytext;
extern Symbol_Table global_symbol_table;

int ln = 1;
void yyerror(const char *msg);
void *allocate(int size);

%}

%union {
    int     int_val;
    float   float_val;
    char    *str_val;
    Decl    decl_val;
    Decls   decls_val;
    Expr    expr_val;
    Stmts   stmts_val;
    Stmt    stmt_val;
    Program prog_val;
    
    Proc         proc_val;
    Procs        procs_val;
    Proc_Header  proc_header_val;
    Proc_Body    proc_body_val;
    Params       params_val;
    Param        param_val;
    Intervals    intervals_val;
    Interval     interval_val;
    Expr_List    expr_list_val;
}

%token '(' ')' ';'
%token ASSIGN_TOKEN 
%token DO_TOKEN   
%token ELSE_TOKEN 
%token FALSE_TOKEN 
%token FI_TOKEN    
%token IF_TOKEN    
%token INT_TOKEN 
%token BOOL_TOKEN 
%token OD_TOKEN     
%token READ_TOKEN 
%token THEN_TOKEN 
%token TRUE_TOKEN 
%token WHILE_TOKEN 
%token WRITE_TOKEN

%token AND_TOKEN
%token END_TOKEN
%token FLOAT_TOKEN
%token NOT_TOKEN
%token OR_TOKEN
%token PROC_TOKEN
%token REF_TOKEN
%token VAL_TOKEN
%token NOT_EQUAL_TOKEN
%token LESS_OR_EQUAL_TOKEN
%token GREATER_OR_EQUAL_TOKEN
%token DOTDOT_TOKEN

%token INVALID_TOKEN
%token <int_val> INTEGER_TOKEN
%token <float_val> DECIMAL_TOKEN
%token <str_val> STRING_TOKEN
%token <str_val> IDENT_TOKEN



/* Standard operator precedence */

%right ASSIGN_TOKEN
%left OR_TOKEN
%left AND_TOKEN
%right NOT_TOKEN
%nonassoc '=' '<' '>' NOT_EQUAL_TOKEN LESS_OR_EQUAL_TOKEN GREATER_OR_EQUAL_TOKEN
%left '+' '-' 
%left '*' '/'
%left UNARY_MINUS

%type <prog_val>  program
%type <decls_val> declarations
%type <decl_val>  decl
%type <stmts_val> statements 
%type <stmt_val>  stmt
%type <expr_val>  expr 
%type <expr_val>  bool_expr 

%type <int_val>   assign
%type <int_val>   start_cond
%type <int_val>   start_read
%type <int_val>   start_while
%type <int_val>   start_write
%type <int_val>   get_lineno

%type <int_val>          start_proc
%type <int_val>          end_proc
%type <procs_val>        procs
%type <proc_val>         proc
%type <proc_header_val>  proc_header
%type <proc_body_val>    proc_body
%type <params_val>       params
%type <param_val>        param
%type <int_val>          dotdot
%type <intervals_val>    intervals
%type <interval_val>     interval
%type <expr_list_val>    expr_list;

%start program

%%
/*---------------------------------------------------------------------*/

program 
    : procs
        { 
          parsed_program = allocate(sizeof(struct prog));
          parsed_program->procs = $1;
        }
    ;

declarations
    : decl declarations
        {
          $$ = allocate(sizeof(struct decls));
          $$->first = $1;
          $$->rest = $2;
        }

    | /* empty */
        { $$ = NULL; }
    ;
        
decl
    : INT_TOKEN IDENT_TOKEN ';'
        {
          $$ = allocate(sizeof(struct decl));
          $$->lineno = ln;
          $$->id = $2;
          $$->type = INT_TYPE;
          $$->intervals = NULL;
        }

    | FLOAT_TOKEN IDENT_TOKEN ';'
        {
            $$ = allocate(sizeof(struct decl));
            $$->lineno = ln;
            $$->id = $2;
            $$->type = FLOAT_TYPE;
            $$->intervals = NULL;
        }

    | BOOL_TOKEN IDENT_TOKEN ';'
        {
          $$ = allocate(sizeof(struct decl));
          $$->lineno = ln;
          $$->id = $2;
          $$->type = BOOL_TYPE;
          $$->intervals = NULL;
        }

    | INT_TOKEN IDENT_TOKEN '[' intervals ']' ';'
        {
            $$ = allocate(sizeof(struct decl));
            $$->lineno = ln;
            $$->type = INT_ARRAY_TYPE;
            $$->id = $2;
            $$->intervals = $4;
        }

    | FLOAT_TOKEN IDENT_TOKEN '[' intervals ']' ';'
        {
            $$ = allocate(sizeof(struct decl));
            $$->lineno = ln;
            $$->type = FLOAT_ARRAY_TYPE;
            $$->id = $2;
            $$->intervals = $4;
        }

    | BOOL_TOKEN IDENT_TOKEN '[' intervals ']' ';'
        {
            $$ = allocate(sizeof(struct decl));
            $$->lineno = ln;
            $$->type = BOOL_ARRAY_TYPE;
            $$->id = $2;
            $$->intervals = $4;
        }
    ;

intervals
    : interval ',' intervals
        {
            $$ = allocate(sizeof(struct intervals));
            $$->lineno = $1->lineno;
            $$->first = $1;
            $$->rest = $3;
        }
    | interval
        {
            $$ = allocate(sizeof(struct intervals));
            $$->lineno = $1->lineno;
            $$->first = $1;
            $$->rest = NULL;
        }
    ;

interval
    : INTEGER_TOKEN dotdot INTEGER_TOKEN
        {
            $$ = allocate(sizeof(struct interval));
            $$->lineno = $2;
            $$->left = $1;
            $$->right = $3;
        }
    ;

get_lineno
    : /* empty */
        { $$ = ln; }

assign
    : ASSIGN_TOKEN
        { $$ = ln; }

start_cond
    : IF_TOKEN
        { $$ = ln; }

start_read
    : READ_TOKEN
        { $$ = ln; }

start_while
    : WHILE_TOKEN
        { $$ = ln; }

start_write
    : WRITE_TOKEN
        { $$ = ln; }

dotdot
    : DOTDOT_TOKEN
        { $$ = ln; }

start_proc
    : PROC_TOKEN
        { $$ = ln; }

end_proc
    : END_TOKEN
        { $$ = ln; }

procs
    : proc procs
        {
            $$ = allocate(sizeof(struct procs));
            $$->first = $1;
            $$->rest = $2;
        }
    | proc
        {
            $$ = allocate(sizeof(struct procs));
            $$->first = $1;
            $$->rest = NULL;
        }
    ;

proc
    : proc_header proc_body end_proc
        {
            $$ = allocate(sizeof(struct proc));
            $$->lineno = $1->lineno;
            $$->header = $1;
            $$->body = $2;
            add_proc_to_symbol_table($$, global_symbol_table);
        }
    ;

proc_header
    : start_proc IDENT_TOKEN '(' ')'
        {
            $$ = allocate(sizeof(struct proc_header));
            $$->lineno = $1;
            $$->id = $2;
            $$->params = NULL;
        }

    | start_proc IDENT_TOKEN '(' params ')'
        {
            $$ = allocate(sizeof(struct proc_header));
            $$->lineno = $1;
            $$->id = $2;
            $$->params = $4;
        }

/* The procedure body consists of zero or more local variable
 declarations, followed by a non-empty sequence of statements. */
proc_body
    : declarations statements
        {
            $$ = allocate(sizeof(struct proc_body));
            $$->decls = $1;
            $$->stmts = $2;
        }
    ;

params
    : param ',' params
        {
            $$ = allocate(sizeof(struct params));
            $$->first = $1;
            $$->rest = $3;
        }

    | param
        {
            $$ = allocate(sizeof(struct params));
            $$->first = $1;
            $$->rest = NULL;
        }
    ;

param
    : VAL_TOKEN INT_TOKEN IDENT_TOKEN
        {
            $$ = allocate(sizeof(struct param));
            $$->indicator = PARAM_VAL;
            $$->type = INT_TYPE;
            $$->id = $3;
        }
    | VAL_TOKEN FLOAT_TOKEN IDENT_TOKEN
        {
            $$ = allocate(sizeof(struct param));
            $$->indicator = PARAM_VAL;
            $$->type = FLOAT_TYPE;
            $$->id = $3;
        }
    | VAL_TOKEN BOOL_TOKEN IDENT_TOKEN
        {
            $$ = allocate(sizeof(struct param));
            $$->indicator = PARAM_VAL;
            $$->type = BOOL_TYPE;
            $$->id = $3;
        }
    | REF_TOKEN INT_TOKEN IDENT_TOKEN
        {
            $$ = allocate(sizeof(struct param));
            $$->indicator = PARAM_REF;
            $$->type = INT_TYPE;
            $$->id = $3;
        }
    | REF_TOKEN FLOAT_TOKEN IDENT_TOKEN
        {
            $$ = allocate(sizeof(struct param));
            $$->indicator = PARAM_REF;
            $$->type = FLOAT_TYPE;
            $$->id = $3;
        }
    | REF_TOKEN BOOL_TOKEN IDENT_TOKEN
        {
            $$ = allocate(sizeof(struct param));
            $$->indicator = PARAM_REF;
            $$->type = BOOL_TYPE;
            $$->id = $3;
        }
    ;

statements                             /* non-empty list of statements */
    : stmt statements
        {
          $$ = allocate(sizeof(struct stmts));
          $$->first = $1;
          $$->rest = $2;
        }

    | stmt
        {
          $$ = allocate(sizeof(struct stmts));
          $$->first = $1;
          $$->rest = NULL;
        }

    | error ';' { yyerrok; } statements
        { $$ = $4; }
    ;

stmt
    : IDENT_TOKEN assign expr ';'                  /* assignment */
        {
          $$ = allocate(sizeof(struct stmt));
          $$->lineno = $2;
          $$->kind = STMT_ASSIGN;
          $$->info.assign.asg_id = $1;
          $$->info.assign.asg_expr = $3;
        }

    | IDENT_TOKEN '[' expr_list ']' assign expr ';'
        {
            $$ = allocate(sizeof(struct stmt));
            $$->lineno = $5;
            $$->kind = STMT_ASSIGN;
            $$->info.assign.asg_id = $1;
            $$->info.assign.asg_expr = $6;
            $$->expr_list = $3;
        }

    | start_read IDENT_TOKEN ';'                   /* read command */
        {
          $$ = allocate(sizeof(struct stmt));
          $$->lineno = $1;
          $$->kind = STMT_READ;
          $$->info.read = $2;
        }

    | start_read IDENT_TOKEN '[' expr_list ']' ';'
        {
            $$ = allocate(sizeof(struct stmt));
            $$->lineno = $1;
            $$->kind = STMT_READ;
            $$->info.read = $2;
            $$->expr_list = $4;
        }

    | start_write expr ';'                         /* write command */
        {
          $$ = allocate(sizeof(struct stmt));
          $$->lineno = $1;
          $$->kind = STMT_WRITE;
          $$->info.write = $2;
        }

    | IDENT_TOKEN '(' expr_list ')' ';'
        {
            $$ = allocate(sizeof(struct stmt));
            $$->lineno = ln;
            $$->kind = STMT_CALL;
            $$->info.call = $1;
            $$->expr_list = $3;
        }

    | start_cond expr THEN_TOKEN statements FI_TOKEN
        {
          $$ = allocate(sizeof(struct stmt));
          $$->lineno = $1;
          $$->kind = STMT_COND;
          $$->info.cond.cond = $2;
          $$->info.cond.then_branch = $4;
          $$->info.cond.else_branch = NULL;
        }

    | start_cond expr THEN_TOKEN statements ELSE_TOKEN statements FI_TOKEN
        {
          $$ = allocate(sizeof(struct stmt));
          $$->lineno = $1;
          $$->kind = STMT_COND;
          $$->info.cond.cond = $2;
          $$->info.cond.then_branch = $4;
          $$->info.cond.else_branch = $6;
        }

    | start_while expr DO_TOKEN statements OD_TOKEN
        {
          $$ = allocate(sizeof(struct stmt));
          $$->lineno = $1;
          $$->kind = STMT_WHILE;
          $$->info.loop.cond = $2;
          $$->info.loop.body = $4;
        }
    ;

expr_list
    : expr ',' expr_list
        {
            $$ = allocate(sizeof(struct expr_list));
            $$->first = $1;
            $$->rest = $3;
        }

    | expr
        {
            $$ = allocate(sizeof(struct expr_list));
            $$->first = $1;
            $$->rest = NULL;
        }
    ;

expr
    : '-' get_lineno expr                             %prec UNARY_MINUS
        {
          $$ = allocate(sizeof(struct expr));
          $$->kind = EXPR_UNOP;
          $$->unop = UNOP_MINUS;
          $$->e1 = $3;
          $$->e2 = NULL;
          $$->lineno = $2;
        }

    | expr '+' get_lineno expr
        {
          $$ = allocate(sizeof(struct expr));
          $$->kind = EXPR_BINOP;
          $$->binop = BINOP_ADD;
          $$->e1 = $1;
          $$->e2 = $4;
          $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr '-' get_lineno expr
        {
          $$ = allocate(sizeof(struct expr));
          $$->kind = EXPR_BINOP;
          $$->binop = BINOP_SUB;
          $$->e1 = $1;
          $$->e2 = $4;
          $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr '*' get_lineno expr
        {
          $$ = allocate(sizeof(struct expr));
          $$->kind = EXPR_BINOP;
          $$->binop = BINOP_MUL;
          $$->e1 = $1;
          $$->e2 = $4;
          $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr '/' get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_DIV;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | STRING_TOKEN
        {
            $$ = allocate(sizeof(struct expr));
            $$->lineno = ln;
            $$->kind = EXPR_CONST;
            $$->constant.val.string_val = $1;
            $$->constant.type = STRING_TYPE;
            $$->e1 = NULL;
            $$->e2 = NULL;
        }

    | '(' expr ')'
        { $$ = $2; }

    | '(' error ')'
        { $$ = NULL; }

    | IDENT_TOKEN
        { 
          $$ = allocate(sizeof(struct expr));
          $$->lineno = ln;
          $$->kind = EXPR_ID;
          $$->id = $1;
          $$->e1 = NULL;
          $$->e2 = NULL;
        }

    | IDENT_TOKEN '[' expr_list ']'
        {
            $$ = allocate(sizeof(struct expr));
            $$->lineno = ln;
            $$->kind = EXPR_ID;
            $$->id = $1;
            $$->expr_list = $3;
        }

    | INTEGER_TOKEN
        {
          $$ = allocate(sizeof(struct expr));
          $$->lineno = ln;
          $$->kind = EXPR_CONST;
          $$->constant.val.int_val = $1;
          $$->constant.type = INT_TYPE;
          $$->e1 = NULL;
          $$->e2 = NULL;
        }
        
    | DECIMAL_TOKEN
        {
          $$ = allocate(sizeof(struct expr));
          $$->lineno = ln;
          $$->kind = EXPR_CONST;
          $$->constant.val.float_val = $1;
          $$->constant.type = FLOAT_TYPE;
          $$->e1 = NULL;
          $$->e2 = NULL;
        }

    | bool_expr
        {
            $$ = $1;
        }
    ;

bool_expr
    : TRUE_TOKEN
        {
          $$ = allocate(sizeof(struct expr));
          $$->lineno = ln;
          $$->kind = EXPR_CONST;
          $$->constant.val.bool_val = TRUE;
          $$->constant.type = BOOL_TYPE;
          $$->e1 = NULL;
          $$->e2 = NULL;
        }

    | FALSE_TOKEN
        {
          $$ = allocate(sizeof(struct expr));
          $$->lineno = ln;
          $$->kind = EXPR_CONST;
          $$->constant.val.bool_val = FALSE;
          $$->constant.type = BOOL_TYPE;
          $$->e1 = NULL;
          $$->e2 = NULL;
        }

    | NOT_TOKEN get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_UNOP;
            $$->unop = UNOP_NOT;
            $$->e1 = $3;
            $$->e2 = NULL;
            $$->lineno = $2;
        }

    | expr AND_TOKEN get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_AND;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr OR_TOKEN get_lineno expr
    {
        $$ = allocate(sizeof(struct expr));
        $$->kind = EXPR_BINOP;
        $$->binop = BINOP_OR;
        $$->e1 = $1;
        $$->e2 = $4;
        $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
    }

    | expr '=' get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_EQUAL;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr NOT_EQUAL_TOKEN get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_NOT_EQUAL;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr '<' get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_LESS;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr LESS_OR_EQUAL_TOKEN get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_LESS_OR_EQUAL;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr '>' get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_GREATER;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }

    | expr GREATER_OR_EQUAL_TOKEN get_lineno expr
        {
            $$ = allocate(sizeof(struct expr));
            $$->kind = EXPR_BINOP;
            $$->binop = BINOP_GREATER_OR_EQUAL;
            $$->e1 = $1;
            $$->e2 = $4;
            $$->lineno = $1->lineno == $4->lineno ? $1->lineno : $3;
        }
    ;
%%

/*---------------------------------------------------------------------*/

void 
yyerror(const char *msg) {

    fprintf(stderr, "**** Input line %d, near `%s': %s\n", ln, yytext, msg);
    return;
}

void *
allocate(int size) {

    void    *addr;

    addr = malloc((size_t) size);
    if (addr == NULL) 
        report_error_and_exit("Out of memory");
    return addr;
}

/*---------------------------------------------------------------------*/

