/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */


#ifndef WIZ
#define WIZ

#include "ast.h"
#include "std.h"

extern  char    *izfile;          /* Name of file to parse */
extern  Program parsed_program;

static  void    report_error_and_exit(const char *msg);
extern  void    *checked_malloc(int num_bytes);

#endif /* WIZ */

