/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */


/* ast.h */

/*-----------------------------------------------------------------------
    Definitions for the abstract syntax trees generated for Iz programs.
    For use in the COMP90045 project 2014.
-----------------------------------------------------------------------*/

#ifndef AST_H
#define AST_H

#include "std.h"
//#include "symbol.h"



typedef struct decl   *Decl;
typedef struct decls  *Decls;
typedef struct expr   *Expr;
typedef struct stmts  *Stmts;
typedef struct stmt   *Stmt;
typedef struct prog   *Program;

typedef struct param        *Param;
typedef struct params       *Params;
typedef struct proc_header  *Proc_Header;
typedef struct proc_body    *Proc_Body;
typedef struct proc         *Proc;
typedef struct procs        *Procs;
typedef struct intervals    *Intervals;
typedef struct interval     *Interval;
typedef struct expr_list    *Expr_List;

typedef enum {
    BINOP_ADD, BINOP_SUB, BINOP_MUL, BINOP_DIV,
    BINOP_AND, BINOP_OR,
    BINOP_EQUAL,  BINOP_NOT_EQUAL,  BINOP_LESS,  BINOP_LESS_OR_EQUAL,  BINOP_GREATER,  BINOP_GREATER_OR_EQUAL
} BinOp;

#define BINOP_NAMES "+", "-", "*", "/"

extern const char *binopname[]; 

typedef enum {
    UNOP_MINUS, UNOP_NOT
} UnOp;

#define UNOP_NAMES "-"

extern const char *unopname[];

typedef enum {
    BOOL_TYPE, INT_TYPE, FLOAT_TYPE, STRING_TYPE, BOOL_ARRAY_TYPE, INT_ARRAY_TYPE, FLOAT_ARRAY_TYPE
} Type;

typedef union {
    int    int_val;
    float  float_val;
    BOOL   bool_val;
    char * string_val;
} Value;

typedef struct {
    Type   type;
    Value  val;
} Constant;

typedef enum {
    EXPR_ID, EXPR_CONST, EXPR_BINOP, EXPR_UNOP
} ExprKind;

struct expr_list {
    Expr      first;
    Expr_List rest;
};

struct expr {
    int       lineno;
    ExprKind  kind;
    char      *id;          /* for identifiers */
    Constant  constant;     /* for constant values */
    UnOp      unop;         /* for unary operators */
    BinOp     binop;        /* for binary operators */
    Expr      e1;           /* for unary and binary operators */
    Expr      e2;           /* for binary operators */
    Expr_List expr_list;
};

struct decl {
    int       lineno;
    char      *id;
    Type      type;
    Intervals intervals;
};

struct decls {
    Decl      first;
    Decls     rest;
};

struct interval {
    int       lineno;
    int       left;
    int       right;
};

struct intervals {
    int       lineno;
    Interval  first;
    Intervals rest;
};

typedef enum {
    PARAM_VAL, PARAM_REF
} ParamIndicatorKind;

struct param {
    ParamIndicatorKind  indicator;
    char                *id;
    Type                type;
};

struct params {
    Param     first;
    Params    rest;
};

struct proc_header {
    int       lineno;
    char      *id;
    Params    params;
};

struct proc_body {
    Decls     decls;
    Stmts     stmts;
};

struct proc {
    int         lineno;
    Proc_Header header;
    Proc_Body   body;
};

struct procs {
    Proc      first;
    Procs     rest;
};

typedef enum {
    STMT_ASSIGN, STMT_COND, STMT_READ, STMT_WHILE, STMT_WRITE, STMT_CALL
} StmtKind;

typedef struct {
    char      *asg_id;
    Expr      asg_expr;
} Assign;

typedef struct {
    Expr      cond;
    Stmts     then_branch;
    Stmts     else_branch;
} Cond;

typedef struct {
    Expr      cond;
    Stmts     body;
} While;

typedef union {
    Assign    assign;
    Stmts     stmts;
    Cond      cond;
    char      *read;
    Expr      write;
    While     loop;
    char      *call;
} SInfo;

struct stmt {
    int       lineno;
    StmtKind  kind;
    SInfo     info;
    Expr_List expr_list;
};

struct stmts {
    Stmt      first;
    Stmts     rest;
};

struct prog {
    Procs     procs;
};

extern void print_prog(Program prog);

#endif /* AST_H */
