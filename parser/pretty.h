/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */

#ifndef PRETTY_H
#define PRETTY_H

/* pretty.h */

#include <stdio.h>
#include "std.h"
#include "ast.h"

void    pretty_prog(FILE *fp, Program prog);


#endif /* PRETTY_H */

