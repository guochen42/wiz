
#include    <stdio.h>
#include    <string.h>
#include    "symbol.h"
#include    "ast.h"

Symbol_Table global_symbol_table;

Symbol_Node init_symbol_node(Proc proc);
Symbol_Node get_decls_symbol_nodes_list(Decls decls);
Symbol_Node get_params_symbol_nodes_list(Params params);

void init_value_by_type(Value *value, Type type);


void
init_symbol_table() {
    
    global_symbol_table = checked_malloc(sizeof(struct proc_node));
    INIT_LIST_HEAD(&(global_symbol_table->list_node));
}

void
print_symbol_table(Symbol_Table symbol_table) {
    
    struct list_head *pos;
    list_for_each(pos, &(symbol_table->list_node)) {
        struct proc_node *proc_node = list_entry(pos, struct proc_node, list_node);
        printf("in proc: %s\n", proc_node->proc_info->header->id);
        print_proc_node(proc_node);
    }
}

void
print_proc_node(Proc_Node proc_node) {
    
    struct list_head * pos;
    list_for_each(pos, &(proc_node->symbol_node->list_node)) {
        printf("symbol: %s\n", list_entry(pos, struct symbol_node, list_node)->symbol_info.id);
    }
}

void
add_proc_to_symbol_table(Proc proc, Symbol_Table symbol_table) {
    
    Proc_Node proc_node = checked_malloc(sizeof(struct proc_node));
    proc_node->proc_info = proc;     // NOTICE: This is a shallow copy.
    proc_node->symbol_node = init_symbol_node(proc);
    INIT_LIST_HEAD(&(proc_node->list_node));
    
    list_add(&(proc_node->list_node), &(symbol_table->list_node));
}

Symbol_Node
init_symbol_node(Proc proc) {
    
    Symbol_Node rst;
    Symbol_Node decls_list;
    Symbol_Node params_list;

    if (proc->header->params) {
        params_list = get_params_symbol_nodes_list(proc->header->params);
    } else {
        params_list = init_null_symbol_node();
    }
    
    if (proc->body->decls) {
        decls_list = get_decls_symbol_nodes_list(proc->body->decls);
    } else {
        decls_list = init_null_symbol_node();
    }
    
    list_splice(&(decls_list->list_node), &(params_list->list_node));
    rst = params_list;
    
    return rst;
}

Symbol_Node
get_decls_symbol_nodes_list(Decls decls) {
    
    Symbol_Node rst;
    Symbol_Node new_symbol_node;
    
    new_symbol_node = init_null_symbol_node();
    
    if (decls->rest == NULL) {
        rst = init_null_symbol_node();
    } else {
        rst = get_decls_symbol_nodes_list(decls->rest);
    }
    
    /****************************************************************/
    // modify this part to create symbol_info data.
    new_symbol_node->symbol_info.id = decls->first->id;
    new_symbol_node->symbol_info.type = decls->first->type;
    init_value_by_type(&(new_symbol_node->symbol_info.value), new_symbol_node->symbol_info.type);
    new_symbol_node->symbol_info.indicator = PARAM_REF;
    new_symbol_node->symbol_info.symbol_type = TYPE_PARAM;
    /****************************************************************/
    
    list_add(&(new_symbol_node->list_node), &(rst->list_node));
   
    return rst;
}

Symbol_Node
get_params_symbol_nodes_list(Params params) {
    
    Symbol_Node rst;
    Symbol_Node new_symbol_node;
    
    new_symbol_node = init_null_symbol_node();
    
    if (params->rest == NULL) {
        rst = init_null_symbol_node();
    } else {
        rst = get_params_symbol_nodes_list(params->rest);
    }
    
    /****************************************************************/
    // modify this part to create symbol_info data.
    new_symbol_node->symbol_info.id = params->first->id;
    new_symbol_node->symbol_info.type = params->first->type;
    init_value_by_type(&(new_symbol_node->symbol_info.value), new_symbol_node->symbol_info.type);
//    new_symbol_node->symbol_info.indicator not init in this case
    new_symbol_node->symbol_info.symbol_type = TYPE_LOCAL_VAR;
    /****************************************************************/
    
    list_add(&(new_symbol_node->list_node), &(rst->list_node));
    
    return rst;
}

Proc_Node
search_proc_in_symbol_table(char *proc_name, Symbol_Table symbol_table) {
    struct list_head *pos;
    list_for_each(pos, &(symbol_table->list_node)) {
        struct proc_node *proc_node = list_entry(pos, struct proc_node, list_node);
        if (strcmp(proc_node->proc_info->header->id, proc_name) == 0) {
            return proc_node;
        }
    }
    return NULL;
}

Symbol_Node
search_symbol_in_proc(char *symbol_id, Proc_Node proc_node) {
    struct list_head * pos;
    list_for_each(pos, &(proc_node->symbol_node->list_node)) {
        struct symbol_node *symbol_node = list_entry(pos, struct symbol_node, list_node);
        if (strcmp(symbol_node->symbol_info.id, symbol_id) == 0) {
            return symbol_node;
        }
    }
    return NULL;
}

int get_number_of_symbol_in_proc(Proc_Node proc_node) {
    struct list_head *pos;
    int num = 0;
    list_for_each(pos, &(proc_node->symbol_node->list_node)) {
        num++;
    }
    return num;
}

int get_number_of_proc_in_symbol_table(Symbol_Table symbol_table) {
    struct list_head *pos;
    int num = 0;
    list_for_each(pos, &(symbol_table->list_node)) {
        num++;
    }
    return num;
}

// Helper func, DO NOT call it.
Symbol_Node
init_null_symbol_node() {
    
    Symbol_Node rst = checked_malloc(sizeof(struct symbol_node));
    INIT_LIST_HEAD(&(rst->list_node));
    
    return rst;
}

void
init_value_by_type(Value *value, Type type) {
    
    switch (type)
    {
        case BOOL_TYPE:
            value->bool_val = FALSE;
            break;
        case INT_TYPE:
            value->int_val = 0;
            break;
        case FLOAT_TYPE:
            value->float_val = 0;
            break;
        default:
            break;
    }
}

