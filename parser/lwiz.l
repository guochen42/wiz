/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */


/* liz.l */

/*-----------------------------------------------------------------------
    A flex specification for Iz, a subset of Wiz.
    For use in the COMP90045 project 2014.
-----------------------------------------------------------------------*/

INTEGER -?[0-9]+
DECIMAL -?[0-9]+(\.[0-9])?[0-9]*
STRING  \"[^\n\f\"]+\"
ID      [a-zA-Z_][A-Za-z_0-9]*
nl      [\n\f]
nonl    [^\n\f]

%{

#include <stdlib.h>
#include <string.h>
#include "std.h"
#include "ast.h"
#include "pwiz.h"

#define YY_NO_UNPUT

int yylex(void);
int yywrap(void);

extern int ln;

%}

%%

do              { return DO_TOKEN; }
else            { return ELSE_TOKEN; }
false           { return FALSE_TOKEN; }
fi              { return FI_TOKEN; }
if              { return IF_TOKEN; }
int             { return INT_TOKEN; }
bool            { return BOOL_TOKEN; }
od              { return OD_TOKEN; }
read            { return READ_TOKEN; }
then            { return THEN_TOKEN; }
true            { return TRUE_TOKEN; }
while           { return WHILE_TOKEN; }
write           { return WRITE_TOKEN; }

and             { return AND_TOKEN; }
end             { return END_TOKEN; }
float           { return FLOAT_TOKEN; }
not             { return NOT_TOKEN; }
or              { return OR_TOKEN; }
proc            { return PROC_TOKEN; }
ref             { return REF_TOKEN; }
val             { return VAL_TOKEN; }

{INTEGER}       { yylval.int_val = atoi(yytext); return INTEGER_TOKEN; }
{DECIMAL}       { yylval.float_val = atof(yytext); return DECIMAL_TOKEN; }
{ID}            { yylval.str_val = (char *) strdup(yytext); return IDENT_TOKEN; }

[-+*\/;(),\[\]=<>\"]   { return yytext[0]; }         /* single character tokens */

"!="             { return NOT_EQUAL_TOKEN; }
"<="             { return LESS_OR_EQUAL_TOKEN; }
">="             { return GREATER_OR_EQUAL_TOKEN; }

".."            { return DOTDOT_TOKEN; }

":="            { return ASSIGN_TOKEN; }

"#"{nonl}*{nl}  { ln++; }

{nl}            { ln++; }

[ \t]+          ; /* skip whitespace */

.               { return INVALID_TOKEN; }     /* parser may find a use for this */

{STRING}        { yylval.str_val = (char *) strdup(yytext); return STRING_TOKEN; }

%%

int yywrap() {
    return 1;
}

