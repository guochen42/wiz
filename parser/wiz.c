/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */


/* wiz.c */

/*-----------------------------------------------------------------------
    A driver for a pretty-printer for Iz, a subset of Wiz.
    For use the COMP90045 project 2014.
-----------------------------------------------------------------------*/

#include    <string.h>
#include    <stdlib.h>
#include    "ast.h"
#include    "std.h"
#include    "pretty.h"
#include    "missing.h"
#include    "symbol.h"

const char  *progname;
const char  *iz_infile;
Program     parsed_program = NULL;

extern FILE *yyin;
extern Symbol_Table global_symbol_table;

static void usage(void);
void        report_error_and_exit(const char *msg);
void        *checked_malloc(int num_bytes);

int
main(int argc, char **argv) {

    const char  *in_filename;
    FILE        *fp = stdout;
    BOOL        pretty_print_only;

    progname = argv[0];
    pretty_print_only = FALSE;

    init_symbol_table();

    /* Process command line */

    if ((argc < 2) || (argc > 3)) {
        usage();
        exit(EXIT_FAILURE);
    }

    if (argc == 2)
        in_filename = argv[1];

    if (argc == 3 && streq(argv[1],"-p")) {
        pretty_print_only = TRUE;
        in_filename = argv[2];
    }
    
    iz_infile = in_filename;
    yyin = fopen(in_filename, "r");
    if (yyin == NULL) {
        perror(in_filename);
        exit(EXIT_FAILURE);
    }
    
    if (yyparse() != 0) {
        /* The error message will already have been printed. */
        exit(EXIT_FAILURE);
    }

    print_symbol_table(global_symbol_table);
    
    if (pretty_print_only) 
        pretty_prog(fp, parsed_program);
    else
        report_error_and_exit("Sorry, cannot generate code yet");
    
//    Proc_Node proc_node = search_proc_in_symbol_table("main", global_symbol_table);
//    int num = get_number_of_symbol_in_proc(proc_node);
//    int num = get_number_of_proc_in_symbol_table(global_symbol_table);
//    printf("num of proc: %d", num);
//    Symbol_Node symbol_node = search_symbol_in_proc("m", proc_node);
//    printf("%s", proc_node->proc_info->header->id);
//    printf("%s", symbol_node->symbol_info.test_str);

    return 0;
}

/*---------------------------------------------------------------------*/

static void
usage(void) {

    printf("usage: wiz [-p] iz_source_file\n");
}

void
report_error_and_exit(const char *msg) {

    fprintf(stderr, "Error: %s\n", msg);
    exit(EXIT_FAILURE);
}

void *
checked_malloc(int num_bytes) {

    void *addr;

    addr = malloc((size_t) num_bytes);
    if (addr == NULL) 
        report_error_and_exit("Out of memory");
    return addr;
}

/*---------------------------------------------------------------------*/
