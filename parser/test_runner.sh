# Created by Professor Harald Søndergaard 
# Updated by group Code Farmer
#
# Name           ID
# GUO, Chen      578218
# ZHU, Lijun     645266
# HE, Xudong     682470
# LI, Sirui      629461
# SUN, Weixing   355419


#!/bin/bash
for file in `ls test_cases/*.wiz`
do
    ./wiz -p $file 2>&1 | diff -u - test_cases/`basename $file`m
    result=$?
    if [ $result -eq 0 ]; then
        echo "$file ok"
    else
        echo "********* Output differ from test case $file *********"
    fi
done
