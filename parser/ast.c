/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */


/* ast.c */

/*-----------------------------------------------------------------------
    Definitions for abstract syntax trees generated for Iz programs.
    (The bulk of these can be found in the header file ast.h).
    For use the COMP90045 project 2014.
-----------------------------------------------------------------------*/

#include    "ast.h"
#include    <stdio.h>



