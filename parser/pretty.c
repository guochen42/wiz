/*
 * Created by Professor Harald Søndergaard 
 * Updated by group Code Farmer
 *
 * Name           ID
 * GUO, Chen      578218
 * ZHU, Lijun     645266
 * HE, Xudong     682470
 * LI, Sirui      629461
 * SUN, Weixing   355419
 */


/* pretty.c */

/*-----------------------------------------------------------------------
    A stub for a pretty-printer for Iz programs.
    For use in the COMP90045 project 2014.
-----------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "ast.h"
#include "pretty.h"


#define INDENT_VALUE 4
#define RESET_INDENT global_Indentations = 0
#define INCREASE_INDENT global_Indentations += INDENT_VALUE
#define DECREASE_INDENT global_Indentations -= INDENT_VALUE

/* The pretty-printer will need to know how to print operators: */

const char  *binopname[] = { BINOP_NAMES };
const char  *unopname[]  = { UNOP_NAMES };

unsigned int global_Indentations = 0;


void    fprint_prog(FILE *fp, Program prog);
void    fprint_decl(FILE *fp, Decl decl);
void    fprint_decls(FILE *fp, Decls decls);
void    fprint_procs(FILE *fp, Procs procs);
void    fprint_expr_with_parentheses(FILE *fp, Expr expr);
void    fprint_expr(FILE *fp, Expr expr);
void    fprint_assign(FILE *fp, Stmt stmt_assign);
void    fprint_cond(FILE *fp, Cond cond);
void    fprint_read(FILE *fp, Stmt stmt_read);
void    fprint_while(FILE *fp, While w);
void    fprint_write(FILE *fp, Expr write);
void    fprint_call(FILE *fp, Stmt stmt_call);
void    fprint_stmt(FILE *fp, Stmt stmt);
void    fprint_stmts(FILE *fp, Stmts stmts);

Proc    get_first_proc_in_procs(Procs procs);


void
pretty_prog(FILE *fp, Program prog) {

    fprint_prog(fp, prog);
}

void
fprint_indentations(FILE *fp) {
    unsigned int count = global_Indentations;
    while (count--) {
        fprintf(fp, " ");
    }
}

void
fprint_prog(FILE *fp, Program prog) {

    if (prog) {
        RESET_INDENT;
        fprint_procs(fp, prog->procs);
        fprintf(fp, "\n");
        RESET_INDENT;
    }
}

void
fprint_type(FILE *fp, Type type) {

    switch (type) {
        case BOOL_TYPE:
            fprintf(fp, "bool");
            break;

        case INT_TYPE:
            fprintf(fp, "int");
            break;

        case FLOAT_TYPE:
            fprintf(fp, "float");
            break;

        case BOOL_ARRAY_TYPE:
            fprintf(fp, "bool");
            break;

        case INT_ARRAY_TYPE:
            fprintf(fp, "int");
            break;

        case FLOAT_ARRAY_TYPE:
            fprintf(fp, "float");
            break;

        default:
            break;
    }
}

void
fprint_interval(FILE *fp, Interval interval) {

    fprintf(fp, "%d..%d", interval->left, interval->right);
}

void
fprint_intervals(FILE *fp, Intervals intervals) {

    if (intervals != NULL) {
        fprint_interval(fp, intervals->first);
        if (intervals->rest != NULL) {
            fprintf(fp, ",");
            fprint_intervals(fp, intervals->rest);
        }
    }
}

void
fprint_decl(FILE *fp, Decl decl) {

    if (decl != NULL) {
        fprint_indentations(fp);
        fprint_type(fp, decl->type);
        fprintf(fp, " %s", decl->id);
        if (decl->intervals) {
            fprintf(fp, "[");
            fprint_intervals(fp, decl->intervals);
            fprintf(fp, "]");
        }
        fprintf(fp, ";");
    }
}

void
fprint_decls(FILE *fp, Decls decls) {

    if (decls) {
        fprint_decl(fp, decls->first);
        fprintf(fp, "\n");
        fprint_decls(fp, decls->rest);
    }
}

void
fprint_param(FILE *fp, Param param) {

    if (param != NULL) {
        switch (param->indicator) {
            case PARAM_VAL:
                fprintf(fp, "val ");
                break;

            case PARAM_REF:
                fprintf(fp, "ref ");
                break;

            default:
                break;
        }

        fprint_type(fp, param->type);
        fprintf(fp, " %s", param->id);
    }
}

void
fprint_params(FILE *fp, Params params) {

    if (params != NULL) {
        fprint_param(fp, params->first);
        if (params->rest != NULL) {
            fprintf(fp, ", ");
            fprint_params(fp, params->rest);
        }
    }
}

void
fprint_proc_header(FILE *fp, Proc_Header header) {

    if (header != NULL) {
        fprintf(fp, "proc %s ", header->id);
        fprintf(fp, "(");
        fprint_params(fp, header->params);
        fprintf(fp, ")");
        fprintf(fp, "\n");
    }
}

void
fprint_proc_body(FILE *fp, Proc_Body body) {

    if (body != NULL) {
        INCREASE_INDENT;
        fprint_decls(fp, body->decls);
        fprintf(fp, "\n");
        fprint_stmts(fp, body->stmts);
        DECREASE_INDENT;
    }
}

void
fprint_proc(FILE *fp, Proc proc) {

    if (proc != NULL) {
        fprint_proc_header(fp, proc->header);
        fprint_proc_body(fp, proc->body);
        fprintf(fp, "end\n\n");
    }
}

void
fprint_lexicographic_in_procs(FILE *fp, Procs procs) {

    if (procs != NULL) {
        Proc first = procs->first;
        Proc first_in_rest = get_first_proc_in_procs(procs->rest);
        if (first_in_rest == NULL) {
            fprint_proc(fp, first);
            fprint_lexicographic_in_procs(fp, procs->rest);
        } else {
            if (strcmp(first->header->id, first_in_rest->header->id) <= 0) {
                fprint_proc(fp, first);
                fprint_lexicographic_in_procs(fp, procs->rest);
            } else {
                fprint_lexicographic_in_procs(fp, procs->rest);
                fprint_proc(fp, first);
            }
        }
    }
}

Proc
get_first_proc_in_procs(Procs procs) {

    if (procs != NULL) {
        Proc first = procs->first;
        Proc first_in_rest = get_first_proc_in_procs(procs->rest);
        if (first_in_rest != NULL)
            return (strcmp(first->header->id, first_in_rest->header->id) <= 0) ? first : first_in_rest;
        else
            return first;
    } else {
        return NULL;
    }
}

void
fprint_procs(FILE *fp, Procs procs) {

    if (procs != NULL) {
        fprint_lexicographic_in_procs(fp, procs);
    }
}

void
fprint_expr_list(FILE *fp, Expr_List expr_list) {

    if (expr_list != NULL) {
        fprint_expr(fp, expr_list->first);
        if (expr_list->rest) {
            fprintf(fp, ", ");
            fprint_expr_list(fp, expr_list->rest);
        }
    }
}

void
fprint_expr_with_parentheses(FILE *fp, Expr expr) {    //Only print parentheses for binop
    if (expr && expr->kind == EXPR_BINOP) {
        fprintf(fp, "(");
        fprint_expr(fp, expr);
        fprintf(fp, ")");
    } else {
        fprint_expr(fp, expr);
    }
}

void
fprint_expr(FILE *fp, Expr expr) {

    if (expr) {
        switch (expr->kind) {
            case EXPR_ID:

                fprintf(fp, "%s", expr->id);
                if (expr->expr_list != NULL) {
                    fprintf(fp, "[");
                    fprint_expr_list(fp, expr->expr_list);
                    fprintf(fp, "]");
                }

                break;

            case EXPR_CONST:

                switch (expr->constant.type) {
                    case BOOL_TYPE:
                        fprintf(fp, expr->constant.val.bool_val ? "true" : "false");
                        break;

                    case INT_TYPE:
                        fprintf(fp, "%d", expr->constant.val.int_val);
                        break;

                    case FLOAT_TYPE:
                        fprintf(fp, "%f", expr->constant.val.float_val);
                        break;

                    case STRING_TYPE:
                        fprintf(fp, "%s", expr->constant.val.string_val);
                        break;

                    default:
                        break;
                }

                break;

            case EXPR_BINOP:

                if (expr->e1 != NULL) {
                    fprint_expr_with_parentheses(fp, expr->e1);
                    fprintf(fp, " ");
                }

                switch (expr->binop) {
                    case BINOP_ADD:
                        fprintf(fp, "+");
                        break;
                    case BINOP_SUB:
                        fprintf(fp, "-");
                        break;
                    case BINOP_MUL:
                        fprintf(fp, "*");
                        break;
                    case BINOP_DIV:
                        fprintf(fp, "/");
                        break;
                    case BINOP_AND:
                        fprintf(fp, "and");
                        break;
                    case BINOP_OR:
                        fprintf(fp, "or");
                        break;
                    case BINOP_EQUAL:
                        fprintf(fp, "=");
                        break;
                    case BINOP_NOT_EQUAL:
                        fprintf(fp, "!=");
                        break;
                    case BINOP_LESS:
                        fprintf(fp, "<");
                        break;
                    case BINOP_LESS_OR_EQUAL:
                        fprintf(fp, "<=");
                        break;
                    case BINOP_GREATER:
                        fprintf(fp, ">");
                        break;
                    case BINOP_GREATER_OR_EQUAL:
                        fprintf(fp, ">=");
                        break;

                    default:
                        break;
                }

                if (expr->e2 != NULL) {
                    fprintf(fp, " ");
                    fprint_expr_with_parentheses(fp, expr->e2);
                }

                break;

            case EXPR_UNOP:

                switch (expr->unop) {
                    case UNOP_MINUS:
                        fprintf(fp, "-");
                        break;

                    case UNOP_NOT:
                        fprintf(fp, "not");
                        break;

                    default:
                        break;
                }

                fprintf(fp, " ");

                if (expr->e1 != NULL) {
                    fprint_expr_with_parentheses(fp, expr->e1);
                }

                break;

            default:
                break;
        }
    }
}

void
fprint_assign(FILE *fp, Stmt stmt_assign) {

    fprint_indentations(fp);
    fprintf(fp, "%s", stmt_assign->info.assign.asg_id);
    if (stmt_assign->expr_list != NULL) {
        fprintf(fp, "[");
        fprint_expr_list(fp, stmt_assign->expr_list);
        fprintf(fp, "]");
    }
    fprintf(fp, " := ");
    fprint_expr(fp, stmt_assign->info.assign.asg_expr);
    fprintf(fp, ";\n");
}

void
fprint_cond(FILE *fp, Cond cond) {

    fprint_indentations(fp);
    fprintf(fp, "if ");
    fprint_expr(fp, cond.cond);
    fprintf(fp, " then\n");
    INCREASE_INDENT;
    fprint_stmts(fp, cond.then_branch);
    DECREASE_INDENT;
    if (cond.else_branch) {
        fprint_indentations(fp);
        fprintf(fp, "else\n");
        INCREASE_INDENT;
        fprint_stmts(fp, cond.else_branch);
        DECREASE_INDENT;
    }
    fprint_indentations(fp);
    fprintf(fp, "fi\n");
}

void
fprint_read(FILE *fp, Stmt stmt_read) {

    if (stmt_read) {
        fprint_indentations(fp);
        fprintf(fp, "read ");
        fprintf(fp, "%s", stmt_read->info.read);
        if (stmt_read->expr_list != NULL) {
            fprintf(fp, "[");
            fprint_expr_list(fp, stmt_read->expr_list);
            fprintf(fp, "]");
        }
        fprintf(fp, ";\n");
    }
}

void
fprint_while(FILE *fp, While w) {

    fprint_indentations(fp);
    fprintf(fp, "while ");
    fprint_expr(fp, w.cond);
    fprintf(fp, " do\n");
    INCREASE_INDENT;
    fprint_stmts(fp, w.body);
    DECREASE_INDENT;
    fprint_indentations(fp);
    fprintf(fp, "od\n");
}

void
fprint_write(FILE *fp, Expr write) {

    if (write) {
        fprint_indentations(fp);
        fprintf(fp, "write ");
        fprint_expr(fp, write);
        fprintf(fp, ";\n");
    }
}

void
fprint_call(FILE *fp, Stmt stmt_call) {

    if (stmt_call) {
        fprint_indentations(fp);
        fprintf(fp, "%s", stmt_call->info.call);
        fprintf(fp, "(");
        fprint_expr_list(fp, stmt_call->expr_list);
        fprintf(fp, ");\n");
    }
}

void
fprint_stmt(FILE *fp, Stmt stmt) {

    if (stmt) {
        switch (stmt->kind) {
            case STMT_ASSIGN:
                fprint_assign(fp, stmt);
                break;

            case STMT_COND:
                fprint_cond(fp, stmt->info.cond);
                break;

            case STMT_READ:
                fprint_read(fp, stmt);
                break;

            case STMT_WHILE:
                fprint_while(fp, stmt->info.loop);
                break;

            case STMT_WRITE:
                fprint_write(fp, stmt->info.write);
                break;

            case STMT_CALL:
                fprint_call(fp, stmt);
                break;

            default:
                break;
        }
    }
}

void
fprint_stmts(FILE *fp, Stmts stmts) {

    if (stmts) {
        fprint_stmt(fp, stmts->first);
        fprint_stmts(fp, stmts->rest);
    }
}
