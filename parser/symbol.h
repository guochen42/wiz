/* symbol.h */

/*-----------------------------------------------------------------------
 
 -----------------------------------------------------------------------*/

#ifndef SYMBOL_H
#define SYMBOL_H

#include "std.h"
#include "list.h"
#include "ast.h"

extern  void    *checked_malloc(int num_bytes);

typedef struct proc_node * Symbol_Table;
typedef struct proc_node * Proc_Node;
typedef struct symbol_node * Symbol_Node;

typedef enum {
    TYPE_PARAM, TYPE_LOCAL_VAR
} Symbol_Type;

typedef struct symbol_info {
    // Add symbol info here
    char *id;
    Type type;
    Value value;
    ParamIndicatorKind indicator;
    Symbol_Type symbol_type;
} Symbol_Info;

struct proc_node {
    struct proc *proc_info;
    Symbol_Node symbol_node;
    struct list_head list_node;
};

struct symbol_node {
    Symbol_Info symbol_info;
    struct list_head list_node;
};

void init_symbol_table();
Symbol_Node init_null_symbol_node();
void add_proc_to_symbol_table(struct proc * proc, Symbol_Table symbol_table);
void print_symbol_table(Symbol_Table symbol_table);
void print_proc_node(Proc_Node proc_node);
Proc_Node search_proc_in_symbol_table(char *proc_name, Symbol_Table symbol_table);    //only return first one
Symbol_Node search_symbol_in_proc(char *symbol_id, Proc_Node proc_node);    //only return first one
int get_number_of_symbol_in_proc(Proc_Node proc_node);
int get_number_of_proc_in_symbol_table(Symbol_Table symbol_table);


#endif /* SYMBOL_H */
